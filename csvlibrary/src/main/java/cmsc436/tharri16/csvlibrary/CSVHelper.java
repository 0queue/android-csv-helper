package cmsc436.tharri16.csvlibrary;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * A class to handle writing and storing row objects
 */

public class CSVHelper<T extends CSVRow> {

    private static final String TAG = CSVHelper.class.getCanonicalName();

    private List<T> rows;
    private Class<T> cls;

    public CSVHelper (Class<T> cls) {
        this.cls = cls;
        rows = new ArrayList<>();
    }

    public void addRow (T row) {
        rows.add(row);
    }

    public List<T> getRows () {
        return rows;
    }

    public void clear () {
        rows.clear();
    }

    /**
     * Write the data to a temporary file.  This file should then
     * be moved to a new location as appropriate
     * @param context the context to get a cache directory from
     * @return the temporary file written to, or null if unable to create or write
     */
    @Nullable
    public File write (@NonNull Context context) {

        PrintWriter printWriter = null;
        File tempFile = null;

        try {
            tempFile = File.createTempFile(TAG, ".csv.tmp", context.getCacheDir());
            printWriter = new PrintWriter(new FileOutputStream(tempFile));
            StringBuilder sb = new StringBuilder();

            for (T row : rows) {
                for (int i = 0; i < row.getNumberOfColumns(); i++) {
                    String s = row.getNth(i);
                    if (s.contains(",")) {
                        Log.e(TAG, "Comma in data, aborting: " + s);
                        return null;
                    }
                    sb.append(s);
                    if (i != row.getNumberOfColumns() - 1) {
                        sb.append(",");
                    }
                }

                sb.append('\n');
            }

            printWriter.print(sb.toString());
        } catch (IOException e) {
            Log.e(TAG, "Failed to create temp file");
            return null;
        } finally {
            if (printWriter != null) {
                printWriter.close();
            }
        }

        return tempFile;
    }

    /**
     * Add the rows from a CSV file to this instance
     * @param f the file to load from
     * @return true if success, false if failure, no rows will be added
     */
    public boolean load (@NonNull File f) {

        T contract;
        List<T> newRows = new ArrayList<>();

        try {
            contract = cls.newInstance();

            if (f.canRead()) {
                try {
                    BufferedReader br = new BufferedReader(new FileReader(f));
                    String line;
                    while ((line = br.readLine()) != null) {
                        String[] columns = line.split(",");
                        if (columns.length == contract.getNumberOfColumns()) {
                            T row = cls.newInstance();
                            for (int i = 0; i < columns.length; i++) {
                                if (!row.setNth(i, columns[i])) {
                                    Log.e(TAG, "Error in column " + i + " of line: " + line);
                                    return false;
                                }
                            }
                            newRows.add(row);
                        } else {
                            Log.e(TAG, "Invalid line: " + line);
                            return false;
                        }
                    }

                    rows.addAll(newRows);
                    return true;

                } catch (FileNotFoundException e) {
                    Log.e(TAG, "File not found: " + f.getAbsolutePath());
                    return false;
                } catch (IOException e) {
                    Log.e(TAG, "IOException: " + e.toString());
                    return false;
                }
            }
        } catch (InstantiationException e) {
            Log.e(TAG, "Class could not be instantiated, make sure there is a zero-argument constructor in the CSVRow subclass!");
            Log.e(TAG, e.toString());
            return false;
        } catch (IllegalAccessException e) {
            Log.e(TAG, "Class could not be instantiated, make sure there is a public zero-argument constructor!");
            return false;
        }

        Log.e(TAG, "Could not read file: " + f.getAbsolutePath());
        return false;
    }
}
