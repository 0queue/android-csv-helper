package cmsc436.tharri16.csvlibrary;

/**
 * Class for the user of the library to override.
 *
 * IMPORTANT: OVERRIDE THE ZERO ARGUMENT CONSTRUCTOR
 */

public abstract class CSVRow {

    /**
     * For checking that a row works with a CSVHelper
     * @return the number of columns provided
     */
    public abstract int getNumberOfColumns ();

    /**
     * Override this to provide a string representation of all your data
     * in the desired order
     * @param n which column is being requested
     * @return A String of your data, containing *no commas*
     */
    public abstract String getNth (int n);

    /**
     * Used when reading files,
     * @param n which column is being read
     * @param data Raw column from the file, to be parsed as desired
     * @return true if success, false if parsing the column fails.
     *
     * When loading a file using {@link CSVHelper} all file loading will stop
     */
    public abstract boolean setNth (int n, String data);

}
