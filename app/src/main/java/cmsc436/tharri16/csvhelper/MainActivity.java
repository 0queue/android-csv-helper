package cmsc436.tharri16.csvhelper;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.File;

import cmsc436.tharri16.csvlibrary.CSVHelper;
import cmsc436.tharri16.csvlibrary.CSVRow;

public class MainActivity extends AppCompatActivity {

    ArrayAdapter<Integer> random_adapter;
    ArrayAdapter<Long> timestamp_adapter;
    CSVHelper<MyRow> csvHelper;

    private static final String FILENAME = "data.csv";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        csvHelper = new CSVHelper<>(MyRow.class);

        random_adapter = new ArrayAdapter<>(this, R.layout.row_item);
        timestamp_adapter = new ArrayAdapter<>(this, R.layout.row_item);
        random_adapter.addAll(1, 2, 3);
        timestamp_adapter.addAll(1000L, 2000L, 3000L);
        ListView lv_random = (ListView) findViewById(R.id.listview_random);
        ListView lv_timestamp = (ListView) findViewById(R.id.listview_timestamp);
        lv_random.setAdapter(random_adapter);
        lv_timestamp.setAdapter(timestamp_adapter);

        findViewById(R.id.new_row_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int n = (int) (Math.random()*1000);
                long t = System.currentTimeMillis();
                random_adapter.add(n);
                timestamp_adapter.add(t);
                csvHelper.addRow(new MyRow(n, t));
            }
        });

        findViewById(R.id.load_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                csvHelper.clear();
                csvHelper.load(new File(getFilesDir(), FILENAME));
                random_adapter.clear();
                timestamp_adapter.clear();

                for (MyRow row : csvHelper.getRows()) {
                    random_adapter.add(row.i);
                    timestamp_adapter.add(row.timestamp);
                }
            }
        });

        findViewById(R.id.save_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File temp = csvHelper.write(MainActivity.this);
                File real = new File(getFilesDir(), FILENAME);
                if (temp != null) {
                    if (!temp.renameTo(real)) {
                        Log.e(getClass().getCanonicalName(), "Move unsuccessful");
                    }
                }
            }
        });
    }

    private static class MyRow extends CSVRow {

        int i = 0;
        long timestamp;

        public MyRow () {
            // pass
        }

        MyRow(int n, long timestamp) {
            i = n;
            this.timestamp = timestamp;
        }

        @Override
        public int getNumberOfColumns() {
            return 2;
        }

        @Override
        public String getNth(int n) {
            switch (n) {
                case 0:
                    return Integer.toString(i);
                case 1:
                    return Long.toString(timestamp);
                default:
                    return "";
            }
        }

        @Override
        public boolean setNth(int n, String data) {

            try {
                switch (n) {
                    case 0:
                        i = Integer.parseInt(data);
                        return true;
                    case 1:
                        timestamp = Long.parseLong(data);
                        return true;
                    default:
                        return false;
                }
            } catch (NumberFormatException nfe) {
                return false;
            }
        }
    }
}
