# CSV Helper

This is a pair of classes to standardize some data collection for a school project.  It is currently a smaller part of a test project to test integration.

## Setup

In the [Downloads](https://bitbucket.org/0queue/android-csv-helper/downloads/) section, download the `.aar` and add to Android Studio as normal.

## Usage

Create a `CSVHelper`, extend `CSVRow` (ensure you have a zero-arg constructor!) and then `addRow()` or `load(File)`.  When ready to write, `write(Context)` will return the name of the temporary file output.  As in the sample app, it is recommended to then to use `renameTo(File)` to keep it around for a while longer.
